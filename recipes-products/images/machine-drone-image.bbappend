
# Override this function to increase the number of inodes on the data partition
create_usr_ext4_img() {
   make_ext4fs -s -l ${USERDATA_SIZE_EXT4} -i 3000000 ${OUTPUT_FILE_EXT4} ${USR_IMAGE_ROOTFS}
   chmod 644 ${OUTPUT_FILE_EXT4}
}

IMAGE_INSTALL_append = " lib32-gstreamer1.0-omx"
IMAGE_INSTALL_append = " lib32-gstreamer1.0-plugins-bad"
