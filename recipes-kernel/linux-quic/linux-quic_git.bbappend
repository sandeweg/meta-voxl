# This recipe modifies the kernel build.
FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

# Include fragment and build list of to merge
# To add more config fragments:
#   - Add the fragment file to the files directory
#   - Add the file name to the SRC_URI, as shown above
#   - Add the file path to the CFG_FRAGMENTS, as shown below

# Modify the config to disable the requirement to sign kernel modules
SRC_URI += "file://disable_MODULE_SIG.cfg"
CFG_FRAGMENTS = "${WORKDIR}/disable_MODULE_SIG.cfg "

# Modify the config to enable UVC Video Support
SRC_URI += "file://enable_UVC_VIDEO.cfg"
CFG_FRAGMENTS += "${WORKDIR}/enable_UVC_VIDEO.cfg "

# Modify the config to enable SMSC LAN7500 Support
SRC_URI += "file://enable_SMSC_LAN7500.cfg"
CFG_FRAGMENTS += "${WORKDIR}/enable_SMSC_LAN7500.cfg "

# Modify the config to enable USB WWAN Support (WNC Module)
SRC_URI += "file://enable_USB_WWAN.cfg"
CFG_FRAGMENTS += "${WORKDIR}/enable_USB_WWAN.cfg "

# Modify the config to enable VIRTUALIZATION (For Docker)
SRC_URI += "file://enable_VIRTUALIZATION.cfg"
CFG_FRAGMENTS += "${WORKDIR}/enable_VIRTUALIZATION.cfg "

# Modify the config for miscellaneous network updates
SRC_URI += "file://misc-network.cfg"
CFG_FRAGMENTS += "${WORKDIR}/misc-network.cfg "

# Modify the config for Microchip LAN78xx drivers
SRC_URI += "file://lan78xx.cfg"
CFG_FRAGMENTS += "${WORKDIR}/lan78xx.cfg "

# Modify the config for FTDI Serial IO
SRC_URI += "file://ftdi_sio.cfg"
CFG_FRAGMENTS += "${WORKDIR}/ftdi_sio.cfg "

# Modify the config for increasing dmesg buffer size
SRC_URI += "file://increase_dmesg_buffer.cfg"
CFG_FRAGMENTS += "${WORKDIR}/increase_dmesg_buffer.cfg "

# Modify the config for USB Serial Option driver
SRC_URI += "file://enable_USB_SERIAL.cfg"
CFG_FRAGMENTS += "${WORKDIR}/enable_USB_SERIAL.cfg "

# Modify the config for CDC_EEM driver
SRC_URI += "file://enable_CDC_EEM.cfg"
CFG_FRAGMENTS += "${WORKDIR}/enable_CDC_EEM.cfg "

# Modify the config for RNDIS host driver
SRC_URI += "file://enable_RNDIS_HOST.cfg"
CFG_FRAGMENTS += "${WORKDIR}/enable_RNDIS_HOST.cfg "

# Include ModalAI patches
SRC_URI += "file://modalAI-WNC-Linux-3.18.patch"
SRC_URI += "file://qcserial.patch"
SRC_URI += "file://qcserial2.patch"
SRC_URI += "file://qcserial3.patch"
SRC_URI += "file://qmi_wwan.patch"
SRC_URI += "file://sierra.patch"
SRC_URI += "file://i2c-debug.patch"
SRC_URI += "file://uvc.patch"
SRC_URI += "file://realsense.patch"
SRC_URI += "file://lan78xx/0001-lan78xx-changes-from-net-next.patch"
SRC_URI += "file://lan78xx/0002-backport-phy_unregister_fixup.patch"
SRC_URI += "file://lan78xx/0003-lan78xx-compilation-fixes.patch"
SRC_URI += "file://lan78xx/0004-backport-phy_device_remove.patch"
SRC_URI += "file://lan78xx/0005-backport-fixed_phy_unregister.patch"
SRC_URI += "file://lan78xx/0006-fixed-phy-support-to-lan78xx.patch"
SRC_URI += "file://lan78xx/0007-revert-3.14-kernel-changes.patch"
SRC_URI += "file://lan78xx/0008-system-suspend-crash-fix.patch"
SRC_URI += "file://lan78xx/0009-Fix-to-handle-mtu-size.patch"
SRC_URI += "file://lan78xx/0010-Fix-for-packet-tx-issues-with-fixed-phy.patch"
SRC_URI += "file://lan78xx/0011-Fixes-for-error-handling.patch"
SRC_URI += "file://lan78xx/0012-version-update.patch"
SRC_URI += "file://sierra_wp760x.patch"
SRC_URI += "file://camera-32-bit-cci-data.patch"
SRC_URI += "file://ax88179_178a.patch"
SRC_URI += "file://option.patch"
SRC_URI += "file://usb_wwan.patch"
SRC_URI += "file://qmi_wwan2.patch"
SRC_URI += "file://fix-pmic-L22-voltage-from-2.5V-to-2.8V.patch"

do_configure_prepend() {
   # We merge the config changes with the default config for the board
   # using merge-config.sh kernel tool

   mergeTool=${S}/scripts/kconfig/merge_config.sh
   confDir=${S}/arch/${ARCH}/configs
   defconf=${confDir}/${KERNEL_CONFIG}

   ${mergeTool} -m -O ${confDir} ${defconf} ${CFG_FRAGMENTS}

   # The output will be named .config. We rename it back to ${defconf} because
   # that's what the rest of do_configure expects
   mv ${confDir}/.config ${defconf}
   bbnote "Writing back the merged config: ${confDir}/.config to ${defconf}"
}
